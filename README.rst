hinkebahn
=========

This is a one-file python2 CLI program to query Deutsche Bahn (and
related) connections from the command line via DB's Movas API.

What sets this apart from other, more refined programs is that it just
requires python 2.5.  In this way, it still runs on stock Maemo on Nokia
N900s and similar un-updatable boxes.  Starting python 2.6, this runs
entriely on the standard library. With python 2.5, you need to install
simplejson_.

.. _simplejson: https://pypi.org/project/simplejson/


Installation
------------

Drop bahnconn.py somewhere in your path, ``chmod +x`` it, perhaps adapt
the hash-bang to point to the right python.  That's it.


Usage
-----

The usage scenario is that you are somewhere and want to figure out how
you will get somewhere else by public transport.  To figure this out,
run::

  bahnconn.py <departure station> <arrival station>

You will usually want to quote station names, so this could be::

  bahnconn.py "Heidelberg Hbf" "Mainz Römisches Theater"

If you use station names Deutsche Bahn knows about, bahnconn.py will
just resolve these; otherwise, you will get a list of stations and will
have to enter a number choosing the one you mean.  bahnconn.py memorises
your choices and will not ask them again.  You can edit these aliases in
~/.bahnconn-aliases.

The program will then print a couple of connections.  Type the number
next to it to get a list of legs (and then again the menu of
connections).

There's the ``--in=h`` option; use it to look for connections ``h``
hours in the future.

Yes, the program *has* that rudimentary a UI.  I feel it works pretty
nicely on the N900 in this way.


Limitations
-----------

This program has a long history, going from web scraping via the Hafas
API to the current Movas API.  I've never had proper documentation for
any of that.

Currently, probably the most severe drawback is that bahnconn.py does
not realise when a train is cancelled.  I will retrofit that when I see
how they mark up the cancelled connections.
