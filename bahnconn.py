#!/usr/local/bin/oldpython
"""
A plain text UI to connections of Deutsche Bahn.

This ought to run down to python 2.5 (which, however, has an additional
dependency simplejson, which isn't necessary on 2.6+).

Distributed under CC0.
"""

# this was running against the HAFAS endpoint for a while, but now
# runs against "Vendo/Movas" (whatever:-).  Ignore any references
# to Hafas that still linger.
# I've found some docs at https://www.postman.com/exmatrikulator/vendo-movas-navigator-api/

from __future__ import with_statement

import datetime
import hashlib
import os
import pprint
import sys
import urllib2

try:
  import json
except ImportError:
  # python 2.5: Fall back to simplejson
  # Get it at https://pypi.org/project/simplejson/
  import simplejson as json


class SkipLeg(Exception):
  """elide a junk leg (like "walk")."""


API_URL = "http://bahnauskunft.tfiu.de/mob/"
#API_URL = "https://app.vendo.noncd.db.de/mob/"

# ["HOCHGESCHWINDIGKEITSZUEGE","INTERCITYUNDEUROCITYZUEGE","INTERREGIOUNDSCHNELLZUEGE","NAHVERKEHRSONSTIGEZUEGE","SBAHNEN","BUSSE","SCHIFFE","UBAHN","STRASSENBAHN","ANRUFPFLICHTIGEVERKEHRE"]
ALL_VEHICLES = ["ALL"]
NO_ICE = ["INTERCITYUNDEUROCITYZUEGE","INTERREGIOUNDSCHNELLZUEGE","NAHVERKEHRSONSTIGEZUEGE","SBAHNEN","BUSSE","SCHIFFE","UBAHN","STRASSENBAHN"]
NO_LONG_DISTANCE = ["INTERREGIOUNDSCHNELLZUEGE","NAHVERKEHRSONSTIGEZUEGE","SBAHNEN","BUSSE","SCHIFFE","UBAHN","STRASSENBAHN"]


class _Names:
  def __init__(self, **kwargs):
    for key, value in kwargs.items():
      setattr(self, key, value)

  def __repr__(self):
    return u"<Station {}>".format(self.name)


def _parse_station(station_dict):
  return _Names(
    name=station_dict["name"],
    id=station_dict["evaNr"],
    station_id=station_dict.get("stationId"),
    location_id=station_dict["locationId"])


class _MyErrorHandler(urllib2.BaseHandler):
  def https_response(self, request, response):
    if not (200 <= response.code < 300):
      raise urllib2.HTTPError(
        request.get_full_url(), response.code,
        response.msg, response.headers, response.fp)
    return response

_HafasOpener = urllib2.OpenerDirector()
_HafasOpener.add_handler(urllib2.HTTPHandler())
_HafasOpener.add_handler(urllib2.HTTPSHandler())
_HafasOpener.add_handler(_MyErrorHandler())


class SimpleMovasClient:
  """A minimal client to the DB Movas API.
  """
  ignoredLegs = ["FUSSWEG"]

  def request(self, path, pars, content_type):
    payload = json.dumps(pars).encode("utf-8")
    this_url = API_URL+path

    req = urllib2.Request(this_url, payload, {
        "content-type": content_type,
        "accept": content_type,
        "x-correlation-ID": "null",
      })
    try:
      response = _HafasOpener.open(req).read()
    except urllib2.URLError, ex:
      print("Error response: %s; reason: %s"%(ex, ex.reason))
      raise

    try:
      return json.loads(response)
    except ValueError:
      pass
    with open("debug.json", "w") as f:
      f.write(response)
    raise ReportableError("No useful response from bahn server.  See"
      " debug.json for what they returned.")

  def journeys(self, from_id, to_id, departure_time, vehicles):
    # vehicles is one of ALL_VEHICLES, NO_ICE, NO_LONG_DISTANCE above
    req = {
	    "autonomeReservierung": False,
	    "einstiegsTypList": [
		    "STANDARD"
	    ],
	    "klasse": "KLASSE_2",
	    "reiseHin": {
		    "wunsch": {
			    "abgangsLocationId": from_id,
			    "verkehrsmittel": vehicles,
			    "fahrradmitnahme": False,
			    "zeitWunsch": {
				    "reiseDatum": departure_time.isoformat().split(".")[0],
				    "zeitPunktArt": "ABFAHRT"
			    },
			    "zielLocationId": to_id,
		    }
	    },
	    "reisendenProfil": {
		    "reisende": [
			    {
				    "ermaessigungen": [
					    "KEINE_ERMAESSIGUNG KLASSENLOS"
				    ],
				    "reisendenTyp": "ERWACHSENER"
			    }
		    ]
	    },
	    "reservierungsKontingenteVorhanden": False
    }

    return self._parse_journeys(self.request(
      "angebote/fahrplan", req,
      "application/x.db.vendo.mob.verbindungssuche.v8+json"))

  def locations(self, term):
    req = {
      "searchTerm": term,
      "locationTypes": ["ALL"],
      "maxResults": 40}
    return self._parse_locations(self.request("location/search", req,
      "application/x.db.vendo.mob.location.v3+json"))

  def _parse_leg(self, leg):
    if leg["typ"] in self.ignoredLegs:
      raise SkipLeg(leg["typ"])

    origin = leg["abgangsOrt"]["name"]
    destination = leg["ankunftsOrt"]["name"]
    prod_name = leg.get("mitteltext")
    if prod_name is None:
      prod_name = leg.get("risZuglaufId")
    departure = _parse_bahntime(leg["abgangsDatum"])
    arrival = _parse_bahntime(leg["ankunftsDatum"])

    return _Names(
        departure_sched=departure,
        departure_real=_parse_bahntime(
          leg.get("ezAbgangsDatum", departure)),
        departure_platform=leg["halte"][0].get("gleis", "?"),
        arrival=arrival,
        arrival_real=_parse_bahntime(
          leg.get("ezAnkunftsDatum", arrival)),
        arrival_platform=leg["halte"][-1].get("gleis", "?"),
        origin=origin,
        destination=destination,
        name=prod_name)

  def _parse_legs(self, legs):
    result = []
    for leg in legs:
      try:
        result.append(self._parse_leg(leg))
      except SkipLeg:
        pass
      except Exception as exc:
        import traceback; traceback.print_exc()
        import pdb;pdb.set_trace()
    return result

  def _parse_journeys(self, raw):
    with open("journeys.json", "w") as f:
      json.dump(raw, f)

    result = []

    for journey in raw['verbindungen']:
      # TODO how do I tell cancelled connections?
      # cancelled?  journeyCancelled?
      conn = journey["verbindung"]
      result.append(
        _Names(
          duration=_parse_bahntime(datetime.timedelta(
            seconds=conn["reiseDauer"])),
          legs=self._parse_legs(conn['verbindungsAbschnitte'])))
      result[-1].date = result[-1].legs[0].departure_real
    return result

  def _parse_locations(self, raw):
    try:
      return [_parse_station(station) for station in raw if "evaNr" in station]
    except:
      with open("bad-station.json", "wb") as f:
        f.write(json.dumps(raw).encode("utf-8"))
      raise


############# End pyhafas shim


class ReportableError(Exception):
  pass


class StationAliases:
  """Persistent aliases of stations.

  These are kept in a file ~/.bahnconn-aliases and are lines
  of alias<TAB>bahn designation.

  You can add these manually for shortcuts, and they are fed when
  disambiguating names.
  """
  def __init__(self):
    self.path = os.path.expanduser("~/.bahnconn-aliases")
    self.translations = {}
    self.load()

  def load(self):
    try:
      with open(self.path) as f:
        for line in f:
          alias, orig = line.decode("utf-8").split("\t")
          self.translations[alias.strip()] = orig.strip()
    except IOError:
      pass

  def add(self, alias, orig):
    with open(self.path, "a") as f:
      f.write(("%s\t%s\n"%(alias, orig)).encode("utf-8"))

  def translate(self, name):
    """returns a translation of name from our translations if there
    is one, name itself otherwise.
    """
    return self.translations.get(name, name)

ALIASES = StationAliases()


def perhaps_strftime(val, fmt="%H:%M"):
  if hasattr(val, "strftime"):
    return val.strftime(fmt)
  elif isinstance(val, datetime.timedelta):
    return "%02d:%02d"%(val.seconds//3600, (val.seconds%3600)//60)
  else:
    return val


def _parse_bahntime(val):
  # we snip off the time zone; let's think about it again when we
  # can rely on newer pythons (fromisoformat does the right thing)
  if not isinstance(val, basestring):
    return val
  return datetime.datetime.strptime(val.split("+")[0], "%Y-%m-%dT%H:%M:%S")


def choose_options_text(options, prompt="Number of choice? "):
  """queries the user for a choice between options using a plain text
  interface.

  Options is a list of (label, val) tuples; the selected tuple is returned.

  This displays the the physically first item last on screen; placing
  items likely popular early in options will hence probably reduce
  scrolling back.
  """
  if not options:
    raise ValueError("Attempt to choose from empty list")

  for index, option in enumerate(reversed(options)):
    print("%d %s"%(len(options)-index, option[0]))

  while True:
    try:
      choice = input(prompt)
      return options[int(choice)-1]
    except (ValueError, IndexError, SyntaxError):
      print(("Valid choices are 1 to %s"%len(options)))


def get_station_id(client, station_name):
  """returns an id for a station name.

  This will ask back unless there's an exact match of station_name in
  hafas' names.
  """
  matches = client.locations(station_name)
  if not matches:
    raise ReportableError("No stations for %s"%station_name)
  elif len(matches)==1:
    return matches[0].id

  for mat in matches:
    if mat.name==station_name:
      return mat.id

  user_choice = choose_options_text([(mat.name, mat.id)
    for mat in matches])
  ALIASES.add(station_name, user_choice[0])

  return user_choice[1]


def get_connections(
    client,
    start_at,
    end_at,
    vehicles,
    args_override=None,
    in_hrs=None):
  """returns a sequence of connection objects for a journey starting
  about now at start_at ending at end_at.

  This may incur some user interaction to disambiguate station names.
  """
  departure_time = datetime.datetime.now()
  if in_hrs:
    departure_time = departure_time+datetime.timedelta(hours=in_hrs)

  from_id = get_station_id(client, ALIASES.translate(start_at))
  to_id = get_station_id(client, ALIASES.translate(end_at))
#  from_id = '8000156'
#  to_id = '8000068'

  return client.journeys(from_id, to_id, departure_time, vehicles)


def format_departure(leg):
  """returns a string for the departure time of leg.
  """
  if leg.departure_real!=leg.departure_sched:
    return "%s (S: %s)"%(
      perhaps_strftime(leg.departure_real),
      perhaps_strftime(leg.departure_sched))
  else:
    return perhaps_strftime(leg.departure_sched)


def get_connection_label(conn):
  """returns a label for a pyhafas connection object
  """
  parts = [format_departure(conn.legs[0])]
  parts.append(perhaps_strftime(conn.duration))
  parts.append("-".join(l.name for l in conn.legs))
  parts.append(
    perhaps_strftime(conn.legs[-1].arrival, "%H:%M"))
  return "  ".join(parts)


def print_legs(conn):
  """prints the individual legs within a connection.
  """
  for leg in conn.legs:
    print("%s  %s(%s)  %s"%(
      format_departure(leg),
      leg.origin,
      leg.departure_platform,
      leg.name))
    print("  > %s  %s(%s)"%(
      perhaps_strftime(leg.arrival, "%H:%M"),
      leg.destination,
      leg.arrival_platform))


def offer_details(connections):
  """shows connections and lets users retrieve details for them.
  """
  options = [(get_connection_label(c), c) for c in connections]
  try:
    while True:
      _, conn = choose_options_text(options,
        "Enter number for details, ^C to exit: ")
      print("")
      print_legs(conn)
      print("")
  except KeyboardInterrupt:
    print("")


def parse_cl():
  from optparse import OptionParser
  parser = OptionParser(
    usage="%prog [options] <depart at> <arrive at>\n"
      "Queries Deutsche Bahn connections.")
  parser.add_option("--in", type="float", metavar="N", dest="in_hrs",
    help="Show connections departing N hours in the future")
  parser.add_option("--no-ice", action="store_true", dest="no_ice",
    help="Suppress connections with ICEs")
  parser.add_option("--local-only", action="store_true", dest="local",
    help="Suppress connections with ICEs and ICs")

  opts, args = parser.parse_args()
  if len(args)!=2:
    parser.print_help()
    sys.exit(1)

  opts.start_at = args[0].decode("utf-8")
  opts.end_at = args[1].decode("utf-8")
  return opts


def main():
#  raw = json.load(open("journeys.json"))
#  offer_details(SimpleMovasClient()._parse_journeys(raw))
#  return

  args = parse_cl()
  vehicles = ALL_VEHICLES
  if args.no_ice:
    vehicles = NO_ICE
  if args.local:
    vehicles = NO_LONG_DISTANCE

  client = SimpleMovasClient()

  connections = get_connections(client,
    args.start_at, args.end_at,
    vehicles,
    in_hrs=args.in_hrs)
  offer_details(connections)


if __name__=="__main__":
  try:
    main()
  except ReportableError, ex:
    print(ex)
    sys.exit(1)

# vim:et:sta:sw=2
